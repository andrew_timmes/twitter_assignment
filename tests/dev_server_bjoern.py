import mocks
from flask import Flask

app = Flask(__name__)

@app.route('/status')
def process():
    return mocks.generate_random_request()

if __name__ == "__main__":
    import bjoern

    bjoern.run(app, "127.0.0.1", 80)
