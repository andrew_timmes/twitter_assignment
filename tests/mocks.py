"""
[summary]

    Returns:
        [type]: [description]

    Yields:
        [type]: [description]
"""


from random import randint

COUNT = 1000

APPS = ("Webapp", "Database", "Cache")


def generate_random_version():
    """[summary]

    Returns:
        [type]: [description]
    """
    return ".".join([str(randint(0, 2)) for _ in range(3)])


def generate_random_app():
    """[summary]

    Returns:
        [type]: [description]
    """
    return f"{APPS[randint(0,2)]}{randint(0,2)}"


def generate_random_request():
    """
    [summary]
    """
    response = {
        "Application": generate_random_app(),
        "Version": generate_random_version(),
        "Uptime": randint(10000, 100000000000000),
        "Success_Count": randint(10000, 100000000000000),
        "Error_Count": randint(10000, 100000000000000),
    }
    response["Request_Count"] = response["Success_Count"] + response["Error_Count"]
    return response


def mock_grouper(_, chunk_size: int, count: int = 1):
    """
    Fake grouper to mock file input (with configurable

    Args:
        _ (Iterable): iterable being mocked away
        chunk_size (int): number of elements to yield per tupl
        count (int): number of elements to loop through

    Yields:
        Generator[Tuple, None, None]: [description]
    """

    while count > 0:
        count -= chunk_size
        yield [str(i) for i in range(chunk_size)]


async def mock_perform_query(*args):
    """
    Returns a mock status response.
    """
    return generate_random_request()


def mock_input_file(*args):
    """[summary]

    Yields:
        [type]: [description]
    """
    i = COUNT
    while i > 0:
        i -= 1
        yield "foo"
