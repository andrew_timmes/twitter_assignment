"""
Integration tests for status_aggregator.
"""

import sys

import mocks

import status_aggregator


def test_run(monkeypatch):
    """[summary]

    Args:
        mocker ([type]): [description]
    """
    monkeypatch.setattr(status_aggregator, "grouper", mocks.mock_grouper)
    monkeypatch.setattr(status_aggregator, "perform_query", mocks.mock_perform_query)
    #monkeypatch.setattr(status_aggregator, "get_input_file", mocks.mock_input_file)
    monkeypatch.setattr(sys, "argv", ["status_aggregator", "--input-file", "specs/servers.txt"])
    args = status_aggregator.parse_args()
    status_aggregator.main(**args)
