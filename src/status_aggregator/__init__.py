"""
status_aggregator.py

This utility will accept a list of servers, poll them for statuses, and produce reports
based on their success rate and error rate per-version.

Author: Andrew Timmes <andrew.timmes@gmail.com>
"""

import argparse
import asyncio
import itertools
import json
import logging
import multiprocessing
from collections import defaultdict
from typing import DefaultDict, Dict, Generator, Iterable, List, TextIO, Tuple, Union

import aiohttp
import tenacity

METRICS = {"Request_Count", "Success_Count"}
REQUIRED_KEYS = {
    "Application",
    "Version",
    "Request_Count",
    "Error_Count",
    "Success_Count",
}


def run_worker(
    hosts: List[str], proto: str = "http", endpoint: str = "status"
) -> Tuple[Dict]:
    """
    Wrapper method to call async routines from synchronous context. Coroutines can't be
    pickled, so this is how we pass async functions to our multiprocessing pool.

    Args:
        hosts (List[str]): List of hostnames to query
        proto (str, optional): Protocol to use for query. Defaults to "http".
        endpoint (str, optional): Name of endpoint to query. Defaults to "status".

    Returns:
        Tuple[Tuple]: results of queries
    """
    return asyncio.run(get_statuses(hosts, proto, endpoint))


async def get_statuses(hosts: List[str], proto: str, endpoint: str) -> List[Dict]:
    """
    Concurrently queries list of hosts given a protocol and endpoint, and returns their
    data.

    Args:
        hosts (List[str]): List of hostnames to query
        proto (str): Protocol to use for query
        endpoint (str): Name of endpoint to query

    Returns:
        Tuple[Dict]: [description]
    """
    requests: List[asyncio.Task] = []
    async with aiohttp.ClientSession() as session:
        for host in hosts:
            host = host.strip()
            url: str = f"{proto}://{host}/{endpoint}"
            requests.append(asyncio.ensure_future(perform_query(url, session)))
        responses: List[Dict] = [
            r
            for r in await asyncio.gather(*requests, return_exceptions=True)
            if isinstance(r, dict)
        ]
    return responses


@tenacity.retry(
    retry=tenacity.retry_if_result(lambda x: x is None),
    wait=tenacity.wait_exponential(multiplier=1, min=1, max=10)
    + tenacity.wait_random(0, 2),
    stop=tenacity.stop_after_attempt(5),
)
async def perform_query(url: str, session: aiohttp.ClientSession) -> Union[Dict, None]:
    """
    Queries a single URL with the provided session object.

    If we detect a bad response or bad data, we return None (we have too much data to
    collect to kill an entire collection because of one bad endpoint).

    This is an external call, so we perform retries in case of failed queries with
    exponential backoff and random jitter to prevent every call from happening at once.


    Args:
        url (str): [description]
        session (aiohttp.ClientSession): [description]

    Returns:
        Union[Dict, None]: JSON response of status endpoint if successful, None if not.
    """
    try:
        response: aiohttp.ClientResponse
        async with session.get(url, timeout=10) as response:
            try:
                assert 200 <= response.status < 300
            except AssertionError:
                logging.error("Non-2XX response code from %s: %s", url, response.status)
                return None
            result: dict = await response.json()
            try:
                assert all(key in result for key in REQUIRED_KEYS)
            except AssertionError:
                logging.error(
                    "Response from %s missing one or more attributes: %s",
                    url,
                    [key for key in REQUIRED_KEYS if key not in result],
                )
                return None
            try:
                assert (
                    result["Success_Count"] + result["Error_Count"]
                    == result["Request_Count"]
                )
            except AssertionError:
                logging.error(
                    "Response from %s not coherent: %s successes and %s errors don't "
                    "equal %s requests",
                    url,
                    result["Success_Count"],
                    result["Error_Count"],
                    result["Request_Count"],
                )
                return None
            return result
    except aiohttp.ClientError as exc:
        logging.error("Exception making request to %s: %s", url, str(exc))
        return None


def grouper(iterable: Iterable, chunk_size: int) -> Generator[List, None, None]:
    """
    Given an iterable object and a chunk size, returns a generator that will yield lists
    of <chunk size> elements at a time.

    Args:
        iterable (Iterable): iterable object to wrap
        chunk_size (int): the number of elements to return at a time

    Yields:
        Generator[List]: lists of <chunk size> elements from the original iterator
    """
    iterator: Iterable = iter(iterable)
    while True:
        chunk: List = list(itertools.islice(iterator, chunk_size))
        if not chunk:
            return
        yield chunk


def get_stats_dd() -> DefaultDict:
    """
    Returns a nested DefaultDict loaded with the types needed to compute response
    statistics, for example:

    dd[Application][Version][Metric][Value]
            str       str     str     int

    Returns:
        DefaultDict: with nested types [dict][dict][dict][int]
    """
    stats: DefaultDict = defaultdict(
        lambda: defaultdict(lambda: defaultdict(lambda: 0))
    )
    return stats


def get_input_file(filename: str) -> TextIO:
    """
    Wrapper function for opening an input file.

    Args:
        filename (str): path to input file to open

    Returns:
        TextIO: opened file handle for the given filename
    """
    return open(filename, "r", encoding="utf-8")


def get_output_file(filename: str) -> TextIO:
    """
    Wrapper function for opening an output file. The given path is opened in w+ mode,
    so any existing file in that location will be clobbered.

    Args:
        filename (str): path to input file to open

    Returns:
        TextIO: opened file handle for the given filename
    """
    return open(filename, "w+", encoding="utf-8")


def aggregate_stats(responses: List) -> DefaultDict:
    """
    Given a list of status endpoint responses, aggregates them based on App/Version and
    creates a dictionary with summed metrics (response count, success count, and error
    count) per-app per-version.

    Args:
        responses (List): status endpoint responses

    Returns:
        DefaultDict: [App][Version][Metric][Value] for all combinations of app, version,
        and metric.
    """
    stats: DefaultDict = get_stats_dd()
    for result in [r for r in responses if r]:
        for element in [e for e in result if e]:
            for metric in METRICS:
                stats[element["Application"]][element["Version"]][metric] += element[
                    metric
                ]
    return stats


def calculate_rates(stats: DefaultDict) -> None:
    """
    Given a dictionary of summed metrics, calculates the success rate of each
    App/Version combination.

    Args:
        stats (DefaultDict): summed metrics
    """
    for app in stats:
        for version in stats[app]:
            if stats[app][version]["Request_Count"] > 0:
                stats[app][version]["Success_Rate"] = (
                    stats[app][version]["Success_Count"]
                    / stats[app][version]["Request_Count"]
                    * 100
                )


def output_to_console(stats: DefaultDict) -> None:
    """
    Given a dictionary of computed metrics, outputs it in human-readable format to
    stdout.

    Args:
        stats (DefaultDict): computed metrics
    """
    for app in stats:
        for version in stats[app]:
            if stats[app][version].get("Success_Rate"):
                print(
                    f"{app} version {version} success rate: "
                    f"{stats[app][version]['Success_Rate']}%"
                )


def output_to_file(stats: DefaultDict, filepath: str) -> None:
    """
    Given a dictionary of computed metrics, writes the dictionary in JSON format to the
    specified filename.

    Args:
        stats (DefaultDict): computed metrics
        filepath (str): file path to write json file to
    """
    with get_output_file(filepath) as file:
        file.write(json.dumps(stats))


def main(
    input_file: str = "servers.txt",
    output_file: str = "success_reports.json",
    worker_count: int = None,
    batch_size: int = 100,
    verbose: int = 0,
) -> None:
    """
    Main routine. Given a file listing hostnames, query each hostname's status endpoint,
    then compute and output metrics based on the responses.

    We use a combination of multiprocessing and async HTTP requests to circumvent the
    GIL and maximize throughput.  The list of hostnames is broken into chunks and handed
    out to members of the multiprocessing pool. Results are gathered, and statistics are
    computed locally.

    Args:
        input_file (str, optional): Path to input file listing servers to query.
                                    Defaults to "servers.txt".
        output_file (str, optional): Path to output file . Defaults to "output.txt".
        worker_count (int, optional): Number of multiprocessing workers to spawn.
                                      Defaults to None (which multiprocessing will
                                      interpret to mean "one for every core on the
                                      machine").
        batch_size (int, optional): Number of servers to be handed to each
                                    multiprocessing worker to query concurrently.
                                    Defaults to 100.
        verbose (int, optional): Logging verbosity [0=warning, 1=info, 2+=debug].
                                 Defaults to 0.
    """
    configure_logging(verbose)
    with multiprocessing.Pool(processes=worker_count) as pool:
        with get_input_file(input_file) as file:
            generator: Generator = grouper(file, batch_size)
            tasks: List = []
            for chunk in generator:
                tasks.append(pool.apply_async(run_worker, (chunk,)))
        batch_results: List = [task.get() for task in tasks]
    logging.debug("Total number of responses: %s", sum([len(r) for r in batch_results]))
    stats: DefaultDict = aggregate_stats(batch_results)
    calculate_rates(stats)
    output_to_console(stats)
    output_to_file(stats, output_file)


def configure_logging(loglevel: int) -> None:
    """
    Configures logging.

    Args:
        loglevel (int): value corresponding to desired loglevel
    """
    levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    loglevel = max(loglevel, 0)
    loglevel = min(loglevel, 2)
    logging.basicConfig(level=levels[loglevel], format="%(asctime)s %(message)s")


def parse_args() -> Dict:
    """
    Parses arguments from sys.argv and returns an unpacked dictionary of arguments.

    Returns:
        Dict: configuration options
    """
    argparser = argparse.ArgumentParser(
        description="Poll servers for status metrics and produce aggregated report"
    )
    argparser.add_argument(
        "-i",
        "--input-file",
        action="store",
        type=str,
        default="servers.txt",
        help="path to file containing list of server endpoints",
    )
    argparser.add_argument(
        "-o",
        "--output-file",
        action="store",
        type=str,
        default="success_rates.json",
        help="path to output file",
    )
    argparser.add_argument(
        "-w",
        "--worker-count",
        action="store",
        type=int,
        help="number of workers to spawn",
    )
    argparser.add_argument(
        "-b",
        "--batch-size",
        action="store",
        type=int,
        default=100,
        help="number of tasks workers attempt to grab at once",
    )
    argparser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="set verbosity of logging (can be used multiple times)",
    )

    args: argparse.Namespace = argparser.parse_args()
    return vars(args)


def cli_entrypoint() -> None:
    """
    CLI entrypoint stub to parse arguments.
    """
    arguments: Dict = parse_args()
    main(**arguments)


if __name__ == "__main__":
    cli_entrypoint()
