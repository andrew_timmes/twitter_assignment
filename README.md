# status_aggregator

By: Andrew Timmes <andrew.timmes@gmail.com>

A utility to poll the status endpoints of a list of servers given by an input file and aggregate success rates by app and version.

Prints aggregated statistics to stdout and an output file (configurable via command-line flags); also prints exception info to stderr.

# Requirements

* relatively modern Linux (tested on latest Ubuntu LTs and CentOS 8; any distro with Python 3.7+ packages available should work)
* python version >= 3.7
* `poetry` python package

# Setup

This utility uses poetry to manage a virtual environment with requisite dependencies. To set up the poetry venv and install dependencies:
 (dictated in pyproject.toml)
* `poetry build && poetry install`

# Usage

To run with poetry-installed dependencies:

* `poetry run status_aggregator`

Available flags:
* `--help` flag for usage hints
* `--input-file`: path to input file
* `--output-file`: path to output file to write
* `--worker-count`: number of multiprocessing pool workers to spawn
* `--batch-size`: number of server names to pass to a worker at a time

# Assumptions

* Results are aggregatable within a single machine.
    * If this weren't true, I'd probably use some sort of task-scheduling framework like Celery to distribute tasks among multiple machines, but I assumed that that was beyond the scope of a take-home assignment.
* App names are case-sensitive.
* Server names are all resolvable via some method on nsswitch.conf to IPv4 addresses. 
    * I have no reason to believe IPv6 wouldn't work, but I haven't tested specifically for those conditions.
* We're comfortable not getting 100% of the responses 100% of the time and are okay with aggregating on only the good data we receive.
* Endpoints will return relatively sane data in line with responses.txt.
* Endpoints are all located at http://<server_name>/status on port 80.
* The machine we're running on is dedicated to this process and have at least two cores available to this process.
* I haven't tuned the default --worker-count or --batch-size values; in a real-world environment I'd benchmark the service against the hardware/VM we'd expect to be running on.
